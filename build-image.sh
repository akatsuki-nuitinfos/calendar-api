#! /bin/bash

./gradlew clean build -x test

JAR_FILE=$(ls build/libs/ | grep "^akatsuki")
echo $JAR_FILE

docker build . --build-arg jar=build/libs/$JAR_FILE -t ezzefiohez/nuitinfos-calendrier
docker push ezzefiohez/nuitinfos-calendrier


curl  -X POST http://146.59.248.65:9000/api/webhooks/e8c9fb48-1068-41bc-b002-8100503fcbe5
