package com.picatsu.akatsuki;

import com.picatsu.akatsuki.models.ERole;
import com.picatsu.akatsuki.models.Role;
import com.picatsu.akatsuki.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkatsukiApiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AkatsukiApiApplication.class, args);
	}

	@Autowired
	RoleRepository roleRepository;

	@Override
	public void run(String... args) {

		Role role = new Role();
		role.setId("1");
		role.setName(ERole.ROLE_USER);
		roleRepository.save(role);


		Role role4 = new Role();
		role4.setId("2");
		role4.setName(ERole.ROLE_ADMIN);
		roleRepository.save(role4);


	}

}
