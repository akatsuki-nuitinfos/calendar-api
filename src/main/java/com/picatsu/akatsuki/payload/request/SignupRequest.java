package com.picatsu.akatsuki.payload.request;

import lombok.Data;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Set;

import javax.validation.constraints.*;

@Component
@Data
@Getter
public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;

    private String lastName;
    private String firstName;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
    
    private Set<String> roles;
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

}
