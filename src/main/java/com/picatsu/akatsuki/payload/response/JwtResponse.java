package com.picatsu.akatsuki.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private String id;
	private String username;
	private String lastName;
	private String firstName;

	private String email;
	private List<String> roles;

	public JwtResponse(String accessToken, String id, String username, String lastName, String firstName,String email, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
		this.roles = roles;
	}


}
