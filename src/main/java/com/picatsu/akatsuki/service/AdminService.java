package com.picatsu.akatsuki.service;


import com.picatsu.akatsuki.models.GroupNames;
import com.picatsu.akatsuki.models.Teacher;
import com.picatsu.akatsuki.models.VEventGroupes;
import com.picatsu.akatsuki.models.VEventTeachers;
import com.picatsu.akatsuki.repository.GroupNamesRepository;
import com.picatsu.akatsuki.repository.TeachersRepository;
import com.picatsu.akatsuki.repository.VEventGroupesRepository;
import com.picatsu.akatsuki.utils.CustomFunctions;
import com.picatsu.akatsuki.repository.VEventTeachersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@Component
@Slf4j
public class AdminService extends AbstractService {

    @Autowired
    private VEventTeachersRepository vEventTeachersRepository;
    @Autowired
    private TeachersRepository teachersRepository;
    @Autowired
    private DataService dataService;
    @Autowired
    private GroupNamesRepository groupNamesRepository;
    @Autowired
    private VEventGroupesRepository vEventGroupesRepository;

    @Autowired
    private CustomFunctions customFunctions;


    public Map<String, List<VEventTeachers>> dumpTeachersData() {

        Map<String, List<VEventTeachers>> newEventMap = new HashMap<>();
        List<String> array = new LinkedList<>();
        List<VEventTeachers> arrayEvents = new LinkedList<>();
        List<VEventTeachers> allArrayEvents = new LinkedList<>();
        String calName = "";
        String uri = "";
        for(Teacher teacher: teachersRepository.findAll()) {

            try{
                calName =  customFunctions.getTeachersCalName(teacher.getFirstName(), teacher.getLastName());
                uri = customFunctions.getTeachersURL(teacher.getLastName()+ "_" +teacher.getFirstName());
                arrayEvents = dataService.getTeachersEventsAsPojo( dataService.getEventsAsArray( uri ), calName );
                allArrayEvents.addAll(arrayEvents);
                log.info("Loading successful for : " +  uri );
            }catch (Exception e) {
                log.info("Cannot find retrieve from vtAgenda for : " +  uri );
            }

            for(VEventTeachers event : arrayEvents) {


                List<VEventTeachers> found = vEventTeachersRepository.findAllBySummaryAndDtStartAndDtEndAndDescriptionAndValidTrue(
                        event.getSummary(),
                        event.getDtStart(),
                        event.getDtEnd(),
                        event.getDescription()
                );

                if( found != null && found.size() > 0 &&  !event.equals( found.get(0) )) {  // Si je trouve pas le meme
                    //vEventTeachersRepository.deleteByCalNameAndUid(found.getCalName(), found.getUid());
                    //vEventTeachersRepository.save(event);
                    if (newEventMap.get(teacher.getFirstName() + "_" + teacher.getLastName()) != null) {
                        newEventMap.get(teacher.getFirstName() + "_" + teacher.getLastName()).add(event);

                    } else {
                        if (event.getCalName().equals(
                                customFunctions.getTeachersCalName(teacher.getFirstName(), teacher.getLastName()))) {

                            List<VEventTeachers> list = new LinkedList<>();
                            list.add(event);
                            newEventMap.put(teacher.getFirstName() + "_" + teacher.getLastName(), list);
                        }
                    }
                }

                if( found == null)
                {

                     // vEventTeachersRepository.save(event);
                    if (newEventMap.get(teacher.getFirstName() + "_" + teacher.getLastName()) != null) {
                        newEventMap.get(teacher.getFirstName() + "_" + teacher.getLastName()).add(event);

                    } else {
                        if (event.getCalName().equals(
                                customFunctions.getTeachersCalName(teacher.getFirstName(), teacher.getLastName()))) {

                            List<VEventTeachers> list = new LinkedList<>();
                            list.add(event);
                            newEventMap.put(teacher.getFirstName() + "_" + teacher.getLastName(), list);
                        }
                    }

                }

            } }
       //vEventTeachersRepository.deleteAll();
        // vEventTeachersRepository.saveAll(allArrayEvents);
        for( VEventTeachers e : allArrayEvents) {
            vEventTeachersRepository.insert(e);
        }
        return newEventMap;
    }

    public Map<String, List<VEventGroupes>> dumpGroupeData()  {
        Map<String, List<VEventGroupes>> newEventMap = new LinkedHashMap<>();
        List<String> array = new LinkedList<>();
        List<VEventGroupes> eventGroupesList = new LinkedList<>();
        List<VEventGroupes> allEventGroupesList = new LinkedList<>();
        for(GroupNames groupNames : groupNamesRepository.findAll()) {

            try{
                array =  dataService.getEventsAsArray( groupNames.getLink() );
                eventGroupesList = dataService.getGroupesEventsAsPojo(array, groupNames.getName());
                allEventGroupesList.addAll(eventGroupesList);
                log.info("Loading successful for  : " + groupNames.getName() );
            }catch (Exception e) {
                System.out.println(e);
                log.info("Unable to retrieve from vtAgenda for : " + groupNames.getLink()  );
            }

            for(VEventGroupes event : eventGroupesList ) {
                VEventGroupes found = vEventGroupesRepository.findByCalNameAndSummaryAndDtStartAndDtEndAndDescription(
                        event.getCalName(),
                        event.getSummary(),
                        event.getDtStart(),
                        event.getDtEnd(),
                        event.getDescription()
                );

                if( found != null && ! event.equals( found  )) {  // Si je trouve pas le meme
                      vEventGroupesRepository.deleteByUidAndCalName(found.getUid(), found.getCalName() );
                      vEventGroupesRepository.save(event);
                    if( newEventMap.get(groupNames.getName()) != null ) {
                        newEventMap.get(groupNames.getName()).add(event);
                    } else {
                        if (event.getCalName().equals(groupNames.getName()) ) {
                            List<VEventGroupes> list  = new LinkedList<>();
                            list.add(event);
                            newEventMap.put(groupNames.getName(), list);

                        }

                    }
                }

                if( found == null){

                   vEventGroupesRepository.save(event);
                    if( newEventMap.get(groupNames.getName()) != null ) {
                        newEventMap.get(groupNames.getName()).add(event);
                    } else {
                        if (event.getCalName().equals(groupNames.getName()) ) {

                            List<VEventGroupes> list  = new LinkedList<>();
                            list.add(event);
                            newEventMap.put(groupNames.getName(), list);

                        }

                    }

                }

            }
        }

        // TODO : verifie si meme creneau + classe prise => delete then insert !!!!!
        //this.reloadGroupes(allEventGroupesList);

        return newEventMap;
    }

    private void reloadGroupes(List<VEventGroupes>  allEventGroupesList) {
       vEventGroupesRepository.deleteAll();
        for( VEventGroupes e : allEventGroupesList) {
            vEventGroupesRepository.insert(e);
        }

    }
}
