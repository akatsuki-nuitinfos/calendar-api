package com.picatsu.akatsuki.service;


import com.picatsu.akatsuki.models.VEventGroupes;
import com.picatsu.akatsuki.models.VEventTeachers;
import com.picatsu.akatsuki.repository.VEventGroupesRepository;
import com.picatsu.akatsuki.repository.VEventTeachersRepository;
import com.picatsu.akatsuki.utils.CustomFunctions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@Component
@Slf4j
public class CalendarService extends AbstractService {

    @Autowired
    private VEventGroupesRepository groupesepository;

    @Autowired
    private VEventTeachersRepository teachersRepository;

    @Autowired
    private CustomFunctions customFunctions;

    public Map<String, List<VEventGroupes>> getGroupeInfosAggregatedByTeacher(String groupe) {

         Map<String, List<VEventGroupes>> eventDescriptions = new HashMap<>();

         for(VEventGroupes event: groupesepository.findAllByCalNameContainingIgnoreCaseAndValidTrue(groupe)) {
             // groupe mean teacherName in our case because i use the same Description model object
                if( eventDescriptions.get(event.getDescription().getGroupe().toUpperCase() ) != null ) {
                   eventDescriptions.get(event.getDescription().getGroupe().toUpperCase()).add(event);
               }
               else {
                   List<VEventGroupes> eventList = new LinkedList<>();
                   eventList.add(event);
                   eventDescriptions.put( event.getDescription().getGroupe().toUpperCase(), eventList);
               }
            }

        return eventDescriptions;
    }


    public List<VEventGroupes> eventsByGroup(String groupName){
        return  groupesepository.findAllByCalNameContainingIgnoreCaseAndValidTrue(groupName);
    }

    public List<VEventTeachers> eventsByTeacher(String firstName, String lastName){
        return  teachersRepository
                .findAllByCalName(
                        customFunctions.getTeachersCalName(firstName, lastName));
    }


}
