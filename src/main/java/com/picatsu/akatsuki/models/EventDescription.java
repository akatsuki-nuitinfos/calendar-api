package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventDescription {



    private String language;
    private String matiere;
    private String type;
    private String groupe;
    private String duree;





    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventDescription that = (EventDescription) o;
        return Objects.equals(language, that.language) &&
                Objects.equals(matiere, that.matiere) &&
                Objects.equals(type, that.type) &&
                Objects.equals(groupe, that.groupe) &&
                Objects.equals(duree, that.duree);
    }

    @Override
    public int hashCode() {
        return Objects.hash(language, matiere, type, groupe, duree);
    }
}
