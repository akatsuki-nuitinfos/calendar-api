package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "teachers")
@Builder
public class Teacher {

    private double cm = 1.0;
    private double td = 1.0;
    private double tp = 1.0;
    private String userId;
    private String lastName;
    private String firstName;
    private String email;
    private String status;
    private String quotite;
    private String department;
    private String composante;
    private String service;
    private String pourcentage;





}
