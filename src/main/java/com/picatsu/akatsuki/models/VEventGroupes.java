package com.picatsu.akatsuki.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "vEventGroupes")
public class VEventGroupes {
    @Id
    private String id;

    private String summary;
    private String calName;
    private Date dtStart;
    private Date dtEnd;
    private String location;
    private EventDescription description;
    private Date dtStamp;
    private String uid;
    private boolean valid;






    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VEventGroupes that = (VEventGroupes) o;
        return Objects.equals(summary, that.summary) &&
                Objects.equals(calName, that.calName) &&
                Objects.equals(dtStart, that.dtStart) &&
                Objects.equals(dtEnd, that.dtEnd) &&
                Objects.equals(location, that.location) &&
                Objects.equals(description, that.description) &&
                Objects.equals(dtStamp, that.dtStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(summary, calName, dtStart, dtEnd, location, description, dtStamp, uid);
    }
}
