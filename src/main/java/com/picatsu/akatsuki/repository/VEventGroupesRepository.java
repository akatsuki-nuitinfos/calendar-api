package com.picatsu.akatsuki.repository;


import com.picatsu.akatsuki.models.EventDescription;
import com.picatsu.akatsuki.models.VEventGroupes;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;


public interface VEventGroupesRepository extends MongoRepository<VEventGroupes, String> {
    boolean existsByCalNameAndAndDtStartAndDtEndAndUid(String calName, Date dtStart, Date dtEnd, String uid);
    boolean existsByUid(String uid);
    boolean existsBySummaryAndDtStartAndDtEndAndDescription(String summary, Date dtStart, Date dtEnd, EventDescription description);
    boolean deleteBySummaryAndDtStartAndDtEndAndDescription(String summary, Date dtStart, Date dtEnd, EventDescription description);
    VEventGroupes findByCalNameAndSummaryAndDtStartAndDtEndAndDescription(String calName, String summary, Date dtStart, Date dtEnd, EventDescription description);
    List<VEventGroupes> findAllByCalNameContainingIgnoreCaseAndValidTrue(String calName);

    VEventGroupes findByUidAndCalName(String uid, String CalName);
    Long deleteByUidAndCalName(String uid, String CalName);

    List<VEventGroupes> findByValidTrue();
    List<VEventGroupes> findAllByValidFalse();

    long deleteAllById(String id);
}
