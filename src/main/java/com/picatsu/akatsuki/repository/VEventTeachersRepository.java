package com.picatsu.akatsuki.repository;



import com.picatsu.akatsuki.models.EventDescription;
import com.picatsu.akatsuki.models.VEventTeachers;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface VEventTeachersRepository extends MongoRepository<VEventTeachers, String> {

    List<VEventTeachers> findAllByCalNameAndDtStartIsGreaterThanEqualAndDtEndIsLessThanEqual(String calName, Date dtStart, Date dtEnd);
    List<VEventTeachers> findAllByCalNameAndCategoriesContainingIgnoreCaseAndDtStartIsGreaterThanEqualAndDtEndIsLessThanEqual(String calName, String categories, Date dtStart, Date dtEnd );

    List<VEventTeachers> findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqual(Date dtStart, Date dtEnd);
    List<VEventTeachers> findAllByCategoriesContainingIgnoreCase(String groupName);

    boolean existsByUid(String uid);
    boolean existsBySummaryAndDtStartAndDtEndAndCategories(String Summary, Date dtStart, Date dtEnd, String categories);
    List<VEventTeachers> findAllBySummaryAndDtStartAndDtEndAndDescriptionAndValidTrue(String Summary, Date dtStart, Date dtEnd, EventDescription description);
    Long deleteByCalNameAndUid(String calName, String uid);
    List<VEventTeachers> findAllByCalName(String calName);

    List<VEventTeachers> findAllByValidFalse();
    long deleteAllById(String id);
}
