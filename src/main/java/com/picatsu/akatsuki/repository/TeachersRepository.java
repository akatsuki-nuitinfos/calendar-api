package com.picatsu.akatsuki.repository;


import com.picatsu.akatsuki.models.Teacher;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TeachersRepository extends MongoRepository<Teacher, String> {
    Teacher findByFirstNameAndLastName(String firstName, String lastName);
    boolean existsTeacherByFirstNameAndLastName(String firstName, String lastName);
    List<Teacher> findAllByLastName(String lastName) ;
    Long deleteAllByFirstNameAndLastName(String firstName, String lastName);
    Long deleteByEmail(String email);
    Teacher findByEmail(String email);
    boolean existsByEmail(String email);

}
