package com.picatsu.akatsuki.controllers;



import com.picatsu.akatsuki.models.GroupNames;
import com.picatsu.akatsuki.repository.GroupNamesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/api/v1/group-names")
@RestController("GroupeController")
@Slf4j
@CrossOrigin
public class GroupeNamesController {


    @Autowired
    private GroupNamesRepository groupNamesRepository;

    @GetMapping(value = "/all")
    public List<GroupNames> getAll(){
        log.info("Get all Groupes ");
        return groupNamesRepository.findAll();
    }

    @PostMapping(value = "/add")
    public HttpStatus addTeacher(@RequestBody GroupNames[] groupNames) {
        log.info("Add  Group ");
        try {
            for(GroupNames g: groupNames) {
                if(! (g.getName().isEmpty() || g.getLink().isEmpty() ) ) {
                    if( !groupNamesRepository.existsByName(g.getName().toUpperCase()) ) {
                        g.setName(g.getName().toUpperCase());
                        groupNamesRepository.save(g);
                    } else {
                        this.updateTeacher(g.getName().toUpperCase(), g.getLink());
                    }
                }
            }
        } catch ( Exception e ) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @PutMapping(value = "/update")
    public GroupNames updateTeacher(@RequestParam String name, @RequestParam String link) {
        log.info("update Groupe Name " + name);
        try {

                GroupNames t = groupNamesRepository.findByName(name.toUpperCase());
                t.setLink(link);
                groupNamesRepository.deleteByName(name.toUpperCase());
                groupNamesRepository.save(t);


        } catch (Exception e) {
            log.warn(e.getMessage());
        }

        return groupNamesRepository.findByName(name.toUpperCase());
    }

    @DeleteMapping(value = "/delete")
    public Long deleteTeacher(@RequestParam String name) {
        log.info("delete Teacher " + name );

        return groupNamesRepository.deleteByName(name.toUpperCase());
    }



}
