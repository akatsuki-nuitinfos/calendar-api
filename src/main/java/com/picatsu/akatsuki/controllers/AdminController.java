package com.picatsu.akatsuki.controllers;




import com.picatsu.akatsuki.service.AdminService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RequestMapping(value = "/api/v1/admin")
@RestController("AdminController")
@Slf4j
@CrossOrigin
public class AdminController {


    @Autowired
    private AdminService adminService;

    @PostMapping(value = "/reload-teachers-data")
    @Operation(summary = "Load All Events for Teachers available in TeacherName-DB")
    public Object loadTeachersData() {
        log.info("Reloading All Teachers Data");

        return this.adminService.dumpTeachersData();
    }


    @PostMapping(value = "/reload-groupes-data")
    @Operation(summary = "Load All Events for Groupes available in GroupesName-DB")
    public Object loadGroupesData()  {
        log.info("Reloading All Groupes Data");

        return this.adminService.dumpGroupeData();
    }



}
