package com.picatsu.akatsuki.controllers;



import com.picatsu.akatsuki.models.VEventGroupes;
import com.picatsu.akatsuki.models.VEventTeachers;
import com.picatsu.akatsuki.repository.GroupNamesRepository;
import com.picatsu.akatsuki.repository.VEventTeachersRepository;
import com.picatsu.akatsuki.service.DataService;
import com.picatsu.akatsuki.utils.CustomFunctions;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RequestMapping(value = "/api/v1/data")
@RestController("DataController")
@Slf4j
@CrossOrigin
public class DataController {

    @Autowired
    private DataService dataService;

    @Autowired
    private VEventTeachersRepository vEventTeachersRepository;

    @Autowired
    private GroupNamesRepository groupNamesRepository;

    @Autowired
    private CustomFunctions customFunctions;

    @GetMapping(value = "/raw-array/{firstname}/{lastname}")
    @Operation(summary = "Getting raw data (ics -> arrayList), deprecated ")
    @Deprecated
    public List<String> getAsArray(@PathVariable String firstname, @PathVariable String lastname )
            throws IOException {

        log.info("Getting raw data, deprecated ");

        return dataService.getEventsAsArray(  customFunctions.getTeachersURL(lastname+"_"+firstname) );
    }


    @GetMapping(value = "/ics-to-json/{firstname}/{lastname}")
    @Operation(summary = "Retrieve Data from VtAgenda in Json/Pojo format")
    @Deprecated
    public List<VEventTeachers> getParsed(@PathVariable String firstname,
                                          @PathVariable String lastname,
                                          @RequestParam(defaultValue = "false", required = false) boolean save )
                                throws IOException {

        log.info("Getting /ics-to-json :  "+ firstname +" " +  lastname + " save ? "+ save);

        List<VEventTeachers> i = dataService.getTeachersEventsAsPojo(
                                    dataService.getEventsAsArray(
                                            customFunctions.getTeachersURL( lastname+"_"+firstname)),
                customFunctions.getTeachersCalName(firstname, lastname) );

        if( save == true) vEventTeachersRepository.saveAll(i);
        return i;

    }

    @GetMapping(value = "/events-raw/{groupName}")
    @Operation(summary = "Getting raw data (ics -> arrayList), deprecated ")
    @Deprecated
    public List<String> getArrayEvents(@PathVariable String groupName) throws IOException {

        log.info("Getting groupName details "+ groupName);

        return dataService.getEventsAsArray(groupNamesRepository.findByName(groupName.toUpperCase()).getLink());
    }

    @GetMapping(value = "/groupes-json/{groupName}")
    @Operation(summary = "Retrieve Data from VtAgenda in Json/Pojo format")
    @Deprecated
    public List<VEventGroupes> getJsonEvents(@PathVariable String groupName) throws IOException {

        log.info("Getting groupName details "+ groupName);

        List<String> listeRaw =  dataService.getEventsAsArray(groupNamesRepository
                                                .findByName(groupName.toUpperCase())
                                                    .getLink());

        return dataService.getGroupesEventsAsPojo(listeRaw, groupName.toUpperCase());

    }


}
