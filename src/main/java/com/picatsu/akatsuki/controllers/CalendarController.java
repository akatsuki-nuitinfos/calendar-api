package com.picatsu.akatsuki.controllers;




import com.picatsu.akatsuki.models.VEventGroupes;
import com.picatsu.akatsuki.models.VEventTeachers;
import com.picatsu.akatsuki.service.CalendarService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "/api/v1/calendar")
@RestController("CalendarController")
@Slf4j
@CrossOrigin
public class CalendarController {

    @Autowired
    private CalendarService calendarService;


    @GetMapping(value = "/events-grouped-by-teacher/{groupName}")
    @Operation(summary = "Given groupName (ex: M2MIAA) -> return list of events aggregated by teacher Name")
    public Map<String, List<VEventGroupes>> getGroupDetails(@PathVariable String groupName) {

        log.info("Getting groupName details "+ groupName);

        return calendarService.getGroupeInfosAggregatedByTeacher(groupName);
    }


    @GetMapping(value = "/events-by-group/{groupName}")
    @Operation(summary = "Given groupName (ex: M2MIAA) -> return list of events ")
    public List<VEventGroupes> getGroupEvents(@PathVariable String groupName) throws IOException {

        log.info("Getting groupName details "+ groupName);
            // [ icsetudiant/m2miaa.ics    ,    m2miaa.ics
        return calendarService.eventsByGroup(groupName) ;
    }



    @GetMapping(value = "/teachers-events/{firstName}/{lastName}")
    @Operation(summary = "Given TeacherName -> return list of events ")
    public List<VEventTeachers> getTeacherEvents(@PathVariable String firstName,
                                                 @PathVariable String lastName ) throws IOException {

        log.info("Getting teachersEvents "+ firstName + " " + lastName );

        return calendarService.eventsByTeacher(firstName, lastName) ;
    }




}
