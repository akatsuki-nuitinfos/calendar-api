package com.picatsu.akatsuki.controllers;



import com.picatsu.akatsuki.models.Teacher;
import com.picatsu.akatsuki.models.TeacherDTO;
import com.picatsu.akatsuki.repository.TeachersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

@RequestMapping(value = "/api/v1/teachers")
@RestController("TeacherController")
@Slf4j
@CrossOrigin
public class TeacherNamesController {

    @Autowired
    private TeachersRepository teachersRepository;


    @GetMapping(value = "/all")
    public List<TeacherDTO> getAll(){
        log.info("Get all Teachers ");
        List<TeacherDTO> list = new LinkedList<>();
        for(Teacher t:  teachersRepository.findAll()) {
            list.add(TeacherDTO.builder()
                                .firstName(t.getFirstName())
                                .lastName(t.getLastName())
                                .email(t.getEmail())
                                .build() );
        }
        return list;
    }

    @PostMapping(value = "/add")
    public Teacher addTeacher(@RequestBody Teacher t) {

        log.info("Add  Teachers " + t.toString());

        if( !teachersRepository.existsByEmail(t.getEmail()) && t.getEmail() != null)
            return teachersRepository.save(t);

        return teachersRepository.findByEmail(t.getEmail());
    }

    @PutMapping(value = "/update")
    public Teacher updateTeacher(@RequestBody Teacher t) {

        log.info("update Teacher " + t.toString());

        if( teachersRepository.existsByEmail(t.getEmail()) ) {
            teachersRepository.deleteByEmail(t.getEmail());
            return teachersRepository.save(t);
        }

        return teachersRepository.findByEmail(t.getEmail());
    }

    @DeleteMapping(value = "/delete")
    public Long deleteTeacher(@RequestParam String email) {

        log.info("delete Teacher " + email );

        return teachersRepository.deleteByEmail(email);
    }
}
